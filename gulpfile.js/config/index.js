

const getPackageJson = require( '../lib/getPackageJson');

const pkg = getPackageJson();

let sftp;
try {
    sftp = require('../../sftp.json');
} catch (ex) {
    sftp = { active : false };
}

let rsync;
try {
    rsync = require('../../rsync.json');
} catch (ex) {
    rsync = { active: false };
}


const config = {};

config.pkg = pkg;

config.srcFolder = pkg.thewatch[0].source;
config.themeName = pkg.name;

config.rsync = rsync;
config.sftp = sftp;

// for gulp src
config.doNotCopyList = [
    '/node_modules/',
    '/node_modules/**/*',
    '/.idea/',
    '/.idea/**/*',
    '/.git/',
    '/.git/**/*'
];

// for rync's exclude parameter
config.rsyncExclude = [
    '/node_modules/',
    '/.idea/',
    '/.git/'
]
module.exports = config;
