const gulp = require('gulp');
const rsync = require('gulp-rsync');
const sftp = require('gulp-sftp');

const config = require('../config/');

const deployRemote = (src, dest) => {

    // add src path to the do not copy paths
    const doNotCopy = config.doNotCopyList.map( (path) => {
        return '!' + src + path;
    });
    console.log(doNotCopy);
    console.log('src:' + src);
    console.log('dest:' + dest);
    console.log(config.rsync.active)
    if(config.rsync.active) {
        return gulp.src([ src + '/**/*' ].concat(doNotCopy))
            .pipe(rsync({
                hostname: config.rsync.hostname,
                destination: dest + '/',
                root: src + '/',
                username: config.rsync.username,
                port: config.rsync.port,
                incremental: true,
                progress: true,
                recursive: true,
             //   clean: true,
                exclude: config.rsyncExclude,
                silent: false,
                verbose: true
            }))

    }

    if(config.sftp.active) {
        return gulp.src([src])
            .pipe(sftp( {
                host: config.sftp.hostname,
                user: config.sftp.username,
                pass: config.sftp.pw,
                port: config.sftp.port,
                remotePath: dest + '/',
            }));
    }

    console.log('Did not deploy');
};

module.exports = deployRemote;
