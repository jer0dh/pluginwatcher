

const gulp = require('gulp');
const config = require('../config/');
const deployRemote = require('../lib/deployRemote');


let watch = 0;
config.pkg.thewatch.forEach ( (c) => {

    const doNotCopy = config.doNotCopyList.map( (path) => {
        return '!' + c.source + path;
    });

    const deployName = 'deploy-' + watch;
    ++watch;

    gulp.task( deployName, function(cb) {

        return deployRemote( c.source, c.destination );

    });

    console.log(c.source +'/**/*')
    gulp.watch( [c.source +'/**/*' ].concat(doNotCopy) , [ deployName ]);

});

