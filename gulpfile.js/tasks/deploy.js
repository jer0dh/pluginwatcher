const gulp = require('gulp');
const config = require('../config/');
const rsync = require('gulp-rsync');



gulp.task('deploy', function() {


    if(config.rsync.active) {
        return gulp.src([ config.srcFolder + '/**/*'].concat(config.doNotCopyList))
            .pipe(rsync({
                hostname: config.rsync.hostname,
                destination: config.rsync.destination,
                root: config.srcFolder + '/',
                username: config.rsync.username,
                port: config.rsync.port,
                incremental: true,
                progress: true,
                recursive: true,
                clean: true,
                exclude: config.rsyncExclude
            }))
    }
});
